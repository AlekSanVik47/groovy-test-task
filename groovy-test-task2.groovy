// Задание на умение строить простые алгоритмы

import groovy.json.JsonOutput
import groovy.json.JsonSlurper

def slurper = new JsonSlurper()

def dataMapJson = '{"countTotals":0,"labels":["AVEVA#service$1577104","Capital Project#service$1577101","Internet and WiFi#service$26958","KY serv#service$45416401","LIMS#service$1413208","MES#service$1413207","Naumen Service Desk#service$13150402","New Title#service$1413205","SAP#service$1317704","test#service$13150406"],"series":[{"data":["2902","244","480","59","15","103","93","23957",0,"8"],"name":"Не заполнено"},{"data":["7","7","26",0,"1",0,"1","7",0,0],"name":"Иванов Андрей Валерьевич#employee$1406201"},{"data":["4","2","1",0,0,0,0,0,0,0],"name":"Специалист 1 линии#employee$24148"},{"data":["2",0,0,0,0,0,0,0,0,0],"name":"Салимьянов Марат#employee$62152201"},{"data":["1","2",0,0,0,0,0,0,0,0],"name":"Осипов Артем Алексеевич#employee$22521101"},{"data":["1","6","25","1",0,"1","3","1",0,0],"name":"Чурикова Дарья#employee$5786701"},{"data":["1","23","36","49",0,"1","13","35","2",0],"name":"Степанов Иван#employee$1022702"},{"data":[0,"3","7",0,0,0,"2","6",0,"1"],"name":"Администратор ВКУС#employee$8991701"},{"data":[0,"1","4",0,0,0,"4","12","1","2"],"name":"Дирков Фёдор#employee$24144"},{"data":[0,"1","2",0,0,0,"1","2",0,0],"name":"Кардулин Никита#employee$10190901"},{"data":[0,"1","1",0,0,0,0,"3",0,0],"name":"Иванов Иван#employee$50203"},{"data":[0,0,"10",0,0,0,"1","1",0,0],"name":"Петров Петр Петрович#employee$1602301"},{"data":[0,0,"4","2",0,0,"1","3",0,0],"name":"Ильина Екатерина#employee$55032701"},{"data":[0,0,"2","2",0,0,0,"2",0,0],"name":"Ханов Дамир#employee$10612001"},{"data":[0,0,"2",0,0,0,0,0,0,0],"name":"Сидоров Андрей Валерьевич#employee$1788201"},{"data":[0,0,"1",0,0,0,0,0,0,0],"name":"Аноним, СИБУР-ПЭТФ ППО, АО,СИБУР-ПЭТФ ППО#employee$33189994"},{"data":[0,0,0,"2",0,0,0,0,0,0],"name":"Якунина Ксения#employee$45222701"},{"data":[0,0,0,"1",0,0,0,0,0,"3"],"name":"Наддака Георгий#employee$31702"},{"data":[0,0,0,0,0,0,"1",0,0,0],"name":"Иванов Иван#employee$69038001"},{"data":[0,0,0,0,0,0,0,"3",0,0],"name":"Кусова Марина#employee$99427601"},{"data":[0,0,0,0,0,0,0,"3",0,0],"name":"Петров Константин Валерьевич#employee$1117001"},{"data":[0,0,0,0,0,0,0,"1",0,0],"name":"Засухина Александра Александровна#employee$10612002"},{"data":[0,0,0,0,0,0,0,"1",0,0],"name":"Татарникова Любовь#employee$5782404"},{"data":[0,0,0,0,0,0,0,"1",0,0],"name":"Петров Иван Сидорович#employee$98196801"}]}'
def dataMap = slurper.parseText(dataMapJson)

def dataMapResultJson = '{"countTotals":0,"labels":["SAP#service$1317704","test#service$13150406","LIMS#service$1413208","MES#service$1413207","KY serv#service$45416401","Naumen Service Desk#service$13150402","Capital Project#service$1577101","Internet and WiFi#service$26958","AVEVA#service$1577104","New Title#service$1413205"],"series":[{"data":[0,"8","15","103","59","93","244","480","2902","23957"],"name":"Не заполнено"},{"data":[0,0,"1",0,0,"1","7","26","7","7"],"name":"Иванов Андрей Валерьевич#employee$1406201"},{"data":[0,0,0,0,0,0,"2","1","4",0],"name":"Специалист 1 линии#employee$24148"},{"data":[0,0,0,0,0,0,0,0,"2",0],"name":"Салимьянов Марат#employee$62152201"},{"data":[0,0,0,0,0,0,"2",0,"1",0],"name":"Осипов Артем Алексеевич#employee$22521101"},{"data":[0,0,0,"1","1","3","6","25","1","1"],"name":"Чурикова Дарья#employee$5786701"},{"data":["2",0,0,"1","49","13","23","36","1","35"],"name":"Степанов Иван#employee$1022702"},{"data":[0,"1",0,0,0,"2","3","7",0,"6"],"name":"Администратор ВКУС#employee$8991701"},{"data":["1","2",0,0,0,"4","1","4",0,"12"],"name":"Дирков Фёдор#employee$24144"},{"data":[0,0,0,0,0,"1","1","2",0,"2"],"name":"Кардулин Никита#employee$10190901"},{"data":[0,0,0,0,0,0,"1","1",0,"3"],"name":"Иванов Иван#employee$50203"},{"data":[0,0,0,0,0,"1",0,"10",0,"1"],"name":"Петров Петр Петрович#employee$1602301"},{"data":[0,0,0,0,"2","1",0,"4",0,"3"],"name":"Ильина Екатерина#employee$55032701"},{"data":[0,0,0,0,"2",0,0,"2",0,"2"],"name":"Ханов Дамир#employee$10612001"},{"data":[0,0,0,0,0,0,0,"2",0,0],"name":"Сидоров Андрей Валерьевич#employee$1788201"},{"data":[0,0,0,0,0,0,0,"1",0,0],"name":"Аноним, СИБУР-ПЭТФ ППО, АО,СИБУР-ПЭТФ ППО#employee$33189994"},{"data":[0,0,0,0,"2",0,0,0,0,0],"name":"Якунина Ксения#employee$45222701"},{"data":[0,"3",0,0,"1",0,0,0,0,0],"name":"Наддака Георгий#employee$31702"},{"data":[0,0,0,0,0,"1",0,0,0,0],"name":"Иванов Иван#employee$69038001"},{"data":[0,0,0,0,0,0,0,0,0,"3"],"name":"Кусова Марина#employee$99427601"},{"data":[0,0,0,0,0,0,0,0,0,"3"],"name":"Петров Константин Валерьевич#employee$1117001"},{"data":[0,0,0,0,0,0,0,0,0,"1"],"name":"Засухина Александра Александровна#employee$10612002"},{"data":[0,0,0,0,0,0,0,0,0,"1"],"name":"Татарникова Любовь#employee$5782404"},{"data":[0,0,0,0,0,0,0,0,0,"1"],"name":"Петров Иван Сидорович#employee$98196801"}]}'
def dataMapResult = slurper.parseText(dataMapResultJson)

// Есть некоторая структура данных dataMap (в отформатированном виде можно посмотреть в парсере https://jsonformatter.org/),
// в которой каждому элементу списка "labels" соответствуют значения в списках "data" под соответствующим индексом.
// Необходимо преобразовать структуру таким образом, чтобы элементы списков "labels" и соответствующие элементы списков "data"
// были отсортированы по возрастанию по вычисленной сумме элементов списка "data" соответствующих индексов.

// Решение
dataMap = dataMap as Map

testDataMap = dataMap as Map


dataResult(testDataMap)

def dataResult(Map map) {
    def listLabels
    listLabels = map.get("labels")
    def listData = getTestList(map, "series").each { it }.collect { (it as Map).values().findResult { it } }
    //список значений data
    def indexLabels = listLabels.each { it }.findIndexValues { it } // список индексов labels

    def indexData
    indexData = getlistElementIndicesData(listData)
    def mapSumByIndexData = sumsOfDataToSort(indexLabels, indexData, listData) as Map
    //map с суммами значений data для сортировки
    def mapSort = mapSumByIndexData.sort { it1, it2 -> it2.value <=> it1.value }
    //map с суммами значений data с сортировкой
    def keyMap = mapSort.keySet().collect { (it as List<Number>).collect { it.intValue() } } as List<Integer>
    // ключи сумм значений data c extnjv cjhnbhjdrb

    getMap(map, keyMap)
    resultMap = map
    resultMap = JsonOutput.toJson(map)
}
/*Метод получения итоговой map*/

private Map getMap(Map map, keyMap) {
    getSortListLabels(map, keyMap)
    getSortData(map, keyMap)
    return map
}

/*Метод получения data с сортировкой*/

private void getSortData(Map map, keyMap) {
    def dataSort = map.get("series").each { it }.collect { (it as Map).values().findResult { it } }.collect { getSortList(it as List, keyMap) }
    for (i in 0..<dataSort.size()) {
        def seriesList = map.get("series") as List
        for (j in 0..<seriesList.size()) {
            if (seriesList[j] instanceof Map) {
                def e = seriesList[i] as Map
                if (e.containsKey("data")) {
                    e.replace("data", dataSort[i].each { it })
                }
            }
        }
    }
}

/*Метод получения labels с сортировкой*/

private getSortListLabels(Map map, keyMap) {
    def eSort
    def listLabels
    listLabels = map.get("labels") as List
    def sortLabels = getSortList(listLabels, keyMap) as List
    for (i in 0..<sortLabels.size()) {
        eSort = sortLabels[i]
        for (j in 0..<listLabels.size()) {
            if (i == j) {
                listLabels[j] = eSort
            }
        }
    }
    return listLabels
}

/*Метод sumsOfDataToSort получения сумм data для сортировки
* indexLabels  индексы элементов labels
* indexData список индексов каждого элемента data с не нулевым значением
* listData список всех элементов data*/

private sumsOfDataToSort(indexLabels, indexData, List listData) {
    def listSumData
    def mapSumByIndexData = [:]
    for (i in 0..<indexLabels.size()) {
        def idxL = indexLabels[i] as Integer
        for (j in 0..<indexData.size()) {
            def idxD = indexData[j] as List<Integer>
            for (k in 0..<idxD.size()) {
                def idx = idxD[k] as Integer
                if (idxL == idx) {
                    listSumData = listData.collect { (it as List<Integer>).collect { it.toInteger() }.get(idxL) }.sum()
                    mapSumByIndexData.put(idxL, listSumData)
                }
            }
        }
    }
    mapSumByIndexData

}
/*Метод для сортировки списка*/

private List getSortList(List list, List<Integer> keyMap) {
    def itemSort
    List newListData = []
    for (it in list) {
        List newD = []
        for (j in 0..<keyMap.size()) {
            itemSort = list.get(keyMap[j])
            newD.add(itemSort)
        }
        newListData.add(list.indexOf(it), newD)
        return newListData.collect { (it as List).asReversed() }.collectMany(({ it } as Closure<? extends Collection<?>>))
    }
}

/*Метод получения элемента списка индексов data*/

List getlistElementIndicesData(List indexData) {
    List elements = []
    List el = []
    for (i in 0..<indexData.size()) {
        el = indexData[i].findIndexValues { it }.toList()
        elements.add(el)
    }

    return elements
}
/*метод получения вложенного списка по ключу*/

def getTestList(Map dataMap, def searchKey) {
    dataMap
            .get(searchKey)
}

String jsonMap = dataResult(testDataMap) as String

println("mapResultJson $jsonMap")
dataMap = slurper.parseText(jsonMap)
assert dataMapResult == dataMap
