import groovy.json.JsonOutput
import groovy.json.JsonSlurper

import java.util.stream.Collectors

def slurper = new JsonSlurper()

def dataMapJson = '{"countTotals":0,"labels":["AVEVA#service$1577104","Capital Project#service$1577101","Internet and WiFi#service$26958","KY serv#service$45416401","LIMS#service$1413208","MES#service$1413207","Naumen Service Desk#service$13150402","New Title#service$1413205","SAP#service$1317704","test#service$13150406"],"series":[{"data":["2902","244","480","59","15","103","93","23957",0,"8"],"name":"Не заполнено"},{"data":["7","7","26",0,"1",0,"1","7",0,0],"name":"Иванов Андрей Валерьевич#employee$1406201"},{"data":["4","2","1",0,0,0,0,0,0,0],"name":"Специалист 1 линии#employee$24148"},{"data":["2",0,0,0,0,0,0,0,0,0],"name":"Салимьянов Марат#employee$62152201"},{"data":["1","2",0,0,0,0,0,0,0,0],"name":"Осипов Артем Алексеевич#employee$22521101"},{"data":["1","6","25","1",0,"1","3","1",0,0],"name":"Чурикова Дарья#employee$5786701"},{"data":["1","23","36","49",0,"1","13","35","2",0],"name":"Степанов Иван#employee$1022702"},{"data":[0,"3","7",0,0,0,"2","6",0,"1"],"name":"Администратор ВКУС#employee$8991701"},{"data":[0,"1","4",0,0,0,"4","12","1","2"],"name":"Дирков Фёдор#employee$24144"},{"data":[0,"1","2",0,0,0,"1","2",0,0],"name":"Кардулин Никита#employee$10190901"},{"data":[0,"1","1",0,0,0,0,"3",0,0],"name":"Иванов Иван#employee$50203"},{"data":[0,0,"10",0,0,0,"1","1",0,0],"name":"Петров Петр Петрович#employee$1602301"},{"data":[0,0,"4","2",0,0,"1","3",0,0],"name":"Ильина Екатерина#employee$55032701"},{"data":[0,0,"2","2",0,0,0,"2",0,0],"name":"Ханов Дамир#employee$10612001"},{"data":[0,0,"2",0,0,0,0,0,0,0],"name":"Сидоров Андрей Валерьевич#employee$1788201"},{"data":[0,0,"1",0,0,0,0,0,0,0],"name":"Аноним, СИБУР-ПЭТФ ППО, АО,СИБУР-ПЭТФ ППО#employee$33189994"},{"data":[0,0,0,"2",0,0,0,0,0,0],"name":"Якунина Ксения#employee$45222701"},{"data":[0,0,0,"1",0,0,0,0,0,"3"],"name":"Наддака Георгий#employee$31702"},{"data":[0,0,0,0,0,0,"1",0,0,0],"name":"Иванов Иван#employee$69038001"},{"data":[0,0,0,0,0,0,0,"3",0,0],"name":"Кусова Марина#employee$99427601"},{"data":[0,0,0,0,0,0,0,"3",0,0],"name":"Петров Константин Валерьевич#employee$1117001"},{"data":[0,0,0,0,0,0,0,"1",0,0],"name":"Засухина Александра Александровна#employee$10612002"},{"data":[0,0,0,0,0,0,0,"1",0,0],"name":"Татарникова Любовь#employee$5782404"},{"data":[0,0,0,0,0,0,0,"1",0,0],"name":"Петров Иван Сидорович#employee$98196801"}]}'
def dataMap = slurper.parseText(dataMapJson)

def dataMapResultJson = '{"countTotals":0,"labels":["SAP#service$1317704","test#service$13150406","LIMS#service$1413208","MES#service$1413207","KY serv#service$45416401","Naumen Service Desk#service$13150402","Capital Project#service$1577101","Internet and WiFi#service$26958","AVEVA#service$1577104","New Title#service$1413205"],"series":[{"data":[0,"8","15","103","59","93","244","480","2902","23957"],"name":"Не заполнено"},{"data":[0,0,"1",0,0,"1","7","26","7","7"],"name":"Иванов Андрей Валерьевич#employee$1406201"},{"data":[0,0,0,0,0,0,"2","1","4",0],"name":"Специалист 1 линии#employee$24148"},{"data":[0,0,0,0,0,0,0,0,"2",0],"name":"Салимьянов Марат#employee$62152201"},{"data":[0,0,0,0,0,0,"2",0,"1",0],"name":"Осипов Артем Алексеевич#employee$22521101"},{"data":[0,0,0,"1","1","3","6","25","1","1"],"name":"Чурикова Дарья#employee$5786701"},{"data":["2",0,0,"1","49","13","23","36","1","35"],"name":"Степанов Иван#employee$1022702"},{"data":[0,"1",0,0,0,"2","3","7",0,"6"],"name":"Администратор ВКУС#employee$8991701"},{"data":["1","2",0,0,0,"4","1","4",0,"12"],"name":"Дирков Фёдор#employee$24144"},{"data":[0,0,0,0,0,"1","1","2",0,"2"],"name":"Кардулин Никита#employee$10190901"},{"data":[0,0,0,0,0,0,"1","1",0,"3"],"name":"Иванов Иван#employee$50203"},{"data":[0,0,0,0,0,"1",0,"10",0,"1"],"name":"Петров Петр Петрович#employee$1602301"},{"data":[0,0,0,0,"2","1",0,"4",0,"3"],"name":"Ильина Екатерина#employee$55032701"},{"data":[0,0,0,0,"2",0,0,"2",0,"2"],"name":"Ханов Дамир#employee$10612001"},{"data":[0,0,0,0,0,0,0,"2",0,0],"name":"Сидоров Андрей Валерьевич#employee$1788201"},{"data":[0,0,0,0,0,0,0,"1",0,0],"name":"Аноним, СИБУР-ПЭТФ ППО, АО,СИБУР-ПЭТФ ППО#employee$33189994"},{"data":[0,0,0,0,"2",0,0,0,0,0],"name":"Якунина Ксения#employee$45222701"},{"data":[0,"3",0,0,"1",0,0,0,0,0],"name":"Наддака Георгий#employee$31702"},{"data":[0,0,0,0,0,"1",0,0,0,0],"name":"Иванов Иван#employee$69038001"},{"data":[0,0,0,0,0,0,0,0,0,"3"],"name":"Кусова Марина#employee$99427601"},{"data":[0,0,0,0,0,0,0,0,0,"3"],"name":"Петров Константин Валерьевич#employee$1117001"},{"data":[0,0,0,0,0,0,0,0,0,"1"],"name":"Засухина Александра Александровна#employee$10612002"},{"data":[0,0,0,0,0,0,0,0,0,"1"],"name":"Татарникова Любовь#employee$5782404"},{"data":[0,0,0,0,0,0,0,0,0,"1"],"name":"Петров Иван Сидорович#employee$98196801"}]}'
def dataMapResult = slurper.parseText(dataMapResultJson)

testDataMap = dataMap as Map
def searchKey = "series"
def testList = getTestList(dataMap as Map, searchKey)

static def getVerificationKey(def verificationKey) {
    verificationKey
}

def verificationKey = "data"

/*метод получения вложенного списка по ключу*/

def getTestList(Map dataMap, def searchKey) {
    dataMap
            .get(searchKey)
}

Closure getList
getList = { Map map, def key1 -> map.get(key1) }
searchKey = sortKey(testDataMap)

/*вернуть ключ для сортировки*/

private def sortKey(Map testDataMap) {
    Set searchKey
    searchKey = testDataMap.keySet()
    for (i in 0..<searchKey.size()) {
        try {
            String key = searchKey[i] as String
            def list = testDataMap.get(key) as List
            if (!list.isEmpty()) {
                list.each { j ->
                    j
                    if (j instanceof Map) {
                        j.keySet().each { k ->
                            k
                            def corrKey = isKey(k)
                            if (corrKey) {
                                k
                            }
                        }
                    }
                }
            }
        } catch (MissingMethodException e) {
            String message = e.getMessage()
        }

    }
}

/*проверка корректности ключа для сортировки*/

public static boolean isKey(def correctKey) {
    def verification = "data"
    def isKey
    if (correctKey == verification) {
        isKey = true
    }
}


//def getAllData = testList

//def isKey1 = is(testList)
//listData(testList, isKey)
//testList.collect {isKey1 ? it : null}
/*private def listData(testList, isKey) {
     def list = testList.collect { isKey(it) ? it : null }
}*/

//def desiredKey = dataMap.keySet().each { getTestList(dataMap, it) }.each { it }.collect {}
def listData
def resList
resList = testList

listData = resList.each { it }.collect { (it as Map).values().findResult { it } } // получить все data
def sortData = listData
        .each {
            (it as List<Integer>)
                    .each { v -> v != 0 ? v as Integer : v
                    }
        }
/*
sortData = sortData
        .each { (it as List<Integer>).sort() }
println("sortData: $sortData")
*/

dataResult(testDataMap)

def dataResult(Map map) {
    def listLabels
    listLabels = map.get("labels")
    def listData = getTestList(map, "series").each { it }.collect { (it as Map).values().findResult { it } }
    def indexLabels = listLabels.each { it }.findIndexValues { it }

    println("indexLabels: $indexLabels")
    def indexData
    indexData = getlistElementIndicesData(listData)
    def mapSumByIndexData = sumsOfDataToSort(indexLabels, indexData, listData) as Map
    def mapSort = mapSumByIndexData.sort { it1, it2 -> it2.value <=> it1.value }
    def keyMap = mapSort.keySet().collect {(it as List<Number>).collect {it.intValue()}} as List<Integer>

    println("indexData  " + indexData + "size: " + indexData.size())
    def idxLabels = indexLabels.each { it }
    def idxData = getElementIndexData(indexData) as List

    def getSortListLabels = getSortList(listLabels as List, keyMap)

 //   def sumOfDataByIndex = sortingConditionSumData(idxLabels, idxData, listData) as Map

    dataSorting(listData)

    //resultMap = listData.collect {getSortListData(it as List, keyMap)}//.collectEntries {it as Map}
    getMap(map, keyMap)
   // resultMap = map.get("series").each { it }.collect { (it as Map).values().findResult { it } }.collect {getSortListData(it as List, keyMap)}

    idxLabels = idxLabels.each { it }
    idxLabels = idxData.each { (it as List<Number>).each { it } }

    resultMap = map
    resultMap = JsonOutput.toJson(map)
//        println("listSum $listSum")
    println("resultMap: $resultMap")
}

private Map getMap(Map map, keyMap) {
    getSortListLabels(map, keyMap)
    getSortData(map, keyMap)
    println("map $map")
    return map
}

private void getSortData(Map map, keyMap) {
    def dataSort = map.get("series").each { it }.collect { (it as Map).values().findResult { it } }.collect { getSortList(it as List, keyMap) }
    def eData
    for (i in 0..<dataSort.size()) {
        eData = dataSort[i]
        def seriesList = map.get("series") as List
        for (j in 0..<seriesList.size()) {
            if (seriesList[j] instanceof Map) {
                def e = seriesList[i] as Map
                if (e.containsKey("data")) {
                    e.replace("data", dataSort[i].each { it })
                }
            }
        }
    }
}

private  getSortListLabels(Map map, keyMap) {
    def eSort
    def listLabels
    listLabels = map.get("labels") as List
    def sortLabels = getSortList(listLabels, keyMap) as List
    for (i in 0..<sortLabels.size()) {
        eSort = sortLabels[i]
        for (j in 0..<listLabels.size()) {
            if (i==j) {
                listLabels[j] = eSort
            }
        }
    }
    println("getSortListLabels $listLabels")
   return listLabels
}

/*Метод sumsOfDataToSort получения сумм data для сортировки
* indexLabels  индексы элементов labels
* indexData список индексов каждого элемента data с не нулевым значением
* listData список всех элементов data*/
private sumsOfDataToSort(indexLabels, indexData, List listData) {
    def listSumData
    def mapSumByIndexData = [:]
    for (i in 0..<indexLabels.size()) {
        def idxL = indexLabels[i] as Integer
        for (j in 0..<indexData.size()) {
            def idxD = indexData[j] as List<Integer>
            for (k in 0..<idxD.size()) {
                def idx = idxD[k] as Integer
                if (idxL == idx) {
                    listSumData = listData.collect { (it as List<Integer>).collect { it.toInteger() }.get(idxL) }.sum()
                    mapSumByIndexData.put(idxL, listSumData)
                }
            }
        }
    }
    //def mapSort = mapSumByIndexData.sort { it1, it2 -> it2.value <=> it1.value }
    //def keyMap = mapSort.keySet().collect {(it as List<Number>).collect {it.intValue()}} as List<Integer>
    //keyMap = keyMap.find{it}
    /*def e
    List es = []
    for (i in 0..<listData.size()) {
            e = listData[i] as List
    }*/
//    List newListData = getSortListData(listData, keyMap)
//    listData = newListData


//    mapSort = mapSort.entrySet()
    println("mapSumByIndexData $mapSumByIndexData")
    mapSumByIndexData
//    println("mapSort $mapSort")
//    println("keyMap :  $keyMap" )
//
//    mapSort
}

private List getSortList(List list, List<Integer> keyMap) {
    def itemSort
    List newListData = []
    for (it in list) {
        List newD = []
        for (j in 0..<keyMap.size()) {
            itemSort = list.get(keyMap[j])
            newD.add(itemSort)
        }
        newListData.add(list.indexOf(it), newD)//addAll(newD)
        return newListData.collect {(it as List).asReversed()}.collectMany(({ it } as Closure<? extends Collection<?>>))
    }
}

private List getListItems(List<Integer> keyMap) {
    def elementKeyMap
    def listItemsKeyMap = []
    for (i in 0..<keyMap.size()) {
        elementKeyMap = keyMap[i]
        listItemsKeyMap.add(elementKeyMap)
    }
    listItemsKeyMap
}

private def dataSorting(List listData) {
    listData = elListData(listData)
    boolean isSorted = false
    def buf
    while (!isSorted) {
        isSorted = true
        for (i in 0..<listData.size() - 1) {
            if (listData[i] > listData[i + 1]) {
                isSorted = false
                buf = listData[i]
                listData[i] = listData[i + 1]
                listData[i + 1] = listData[i]
            }
        }
    }
}

private def elListData(List listData) {
    0..<listData.size().each { j ->
        (j as List<Integer>)
                .each {
                    (it.toInteger())
                }
    }
    //listData
}

private def getElementIndexData(List indexData) {

    def idxData = indexData.collectMany { it }

}

def indexData
indexData = getlistElementIndicesData(listData)
getElementIndexData(indexData)

List getlistElementIndicesData(List indexData) {
    List elements = []
    List el = []
    for (i in 0..<indexData.size()) {
        el = indexData[i].findIndexValues { it }.toList()
        elements.add(el)
    }

    return elements
}


//def dataIndexes = listData.each { it }.find().findIndexValues { it } //индексы data
//res2 = res2.sort()
/*def listDataSort = listData.each { it }.each { i -> i }
        .each { j -> j }.get(0) as List<Integer>
listDataSort = listDataSort.each { it as Integer }.sum()*/

//println("resList: " + resList)
//println("listData: " + listData)
//println("dataIndexes: $dataIndexes")

def indicesLabels
def res = dataMap as Map
def listLabels = res.get("labels")

indicesLabels = listLabels.each { it }.findIndexValues { it }


def getIndex(def indices) {
    0..<indices.size() - 1.each { i ->
        indices[i]
    }
}

def compareIndices(def index1, def index2) {
    index1 == index2
}

def result(def newDataMap, def indexLabel, def indexData) {
    if (compareIndices(indexLabel, indexData)) {
        newDataMap.sort {}
    }
}

def testSearchKey
//println("labels: " + listLabels.get(9))
//println("listLabels: $listLabels")
//println("indicesLabels: " + indicesLabels)
//println("lisdataSort: $listDataSort")
//println("getTestList: $testList")
//println("isKey: $isKey")
