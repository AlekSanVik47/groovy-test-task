import groovy.json.JsonSlurper

/*def map = [
        'FF0000' : 'Red',
        '00FF00' : 'Lime',
        '0000FF' : 'Blue',
        'FFFF00' : 'Yellow'
]

map.eachWithIndex { key, val, index ->
    def indent = ((index == 0 || index % 2 == 0) ? "   " : "")
    println "$index Hex Code: $key = Color Name: $val"
}*/
def slurper = new JsonSlurper()
def dataMapJson = '{"countTotals":0,"labels":["AVEVA#service$1577104","Capital Project#service$1577101","Internet and WiFi#service$26958","KY serv#service$45416401","LIMS#service$1413208","MES#service$1413207","Naumen Service Desk#service$13150402","New Title#service$1413205","SAP#service$1317704","test#service$13150406"],"series":[{"data":["2902","244","480","59","15","103","93","23957",0,"8"],"name":"Не заполнено"},{"data":["7","7","26",0,"1",0,"1","7",0,0],"name":"Иванов Андрей Валерьевич#employee$1406201"},{"data":["4","2","1",0,0,0,0,0,0,0],"name":"Специалист 1 линии#employee$24148"},{"data":["2",0,0,0,0,0,0,0,0,0],"name":"Салимьянов Марат#employee$62152201"},{"data":["1","2",0,0,0,0,0,0,0,0],"name":"Осипов Артем Алексеевич#employee$22521101"},{"data":["1","6","25","1",0,"1","3","1",0,0],"name":"Чурикова Дарья#employee$5786701"},{"data":["1","23","36","49",0,"1","13","35","2",0],"name":"Степанов Иван#employee$1022702"},{"data":[0,"3","7",0,0,0,"2","6",0,"1"],"name":"Администратор ВКУС#employee$8991701"},{"data":[0,"1","4",0,0,0,"4","12","1","2"],"name":"Дирков Фёдор#employee$24144"},{"data":[0,"1","2",0,0,0,"1","2",0,0],"name":"Кардулин Никита#employee$10190901"},{"data":[0,"1","1",0,0,0,0,"3",0,0],"name":"Иванов Иван#employee$50203"},{"data":[0,0,"10",0,0,0,"1","1",0,0],"name":"Петров Петр Петрович#employee$1602301"},{"data":[0,0,"4","2",0,0,"1","3",0,0],"name":"Ильина Екатерина#employee$55032701"},{"data":[0,0,"2","2",0,0,0,"2",0,0],"name":"Ханов Дамир#employee$10612001"},{"data":[0,0,"2",0,0,0,0,0,0,0],"name":"Сидоров Андрей Валерьевич#employee$1788201"},{"data":[0,0,"1",0,0,0,0,0,0,0],"name":"Аноним, СИБУР-ПЭТФ ППО, АО,СИБУР-ПЭТФ ППО#employee$33189994"},{"data":[0,0,0,"2",0,0,0,0,0,0],"name":"Якунина Ксения#employee$45222701"},{"data":[0,0,0,"1",0,0,0,0,0,"3"],"name":"Наддака Георгий#employee$31702"},{"data":[0,0,0,0,0,0,"1",0,0,0],"name":"Иванов Иван#employee$69038001"},{"data":[0,0,0,0,0,0,0,"3",0,0],"name":"Кусова Марина#employee$99427601"},{"data":[0,0,0,0,0,0,0,"3",0,0],"name":"Петров Константин Валерьевич#employee$1117001"},{"data":[0,0,0,0,0,0,0,"1",0,0],"name":"Засухина Александра Александровна#employee$10612002"},{"data":[0,0,0,0,0,0,0,"1",0,0],"name":"Татарникова Любовь#employee$5782404"},{"data":[0,0,0,0,0,0,0,"1",0,0],"name":"Петров Иван Сидорович#employee$98196801"}]}'
def dataMap = slurper.parseText(dataMapJson)

dataMap = dataMap as Map
dataMap = dataMap.get("series").each { it }.collect { (it as Map).values().findResult { it } }
println("dataMap $dataMap" + dataMap.getClass().typeName)
dataMap = dataMap.collect {(it as List<Integer>).collect {it.toInteger()}.sum()}
//dataMap = dataMap.collect {it.collect {it.toInteger()}.sum()}.sort()
println("dataMap $dataMap")