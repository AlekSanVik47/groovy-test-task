// Задание на знание основных методов groovy с коллекциями:

def dataMap = [
    'a': [1, 2],
    'b': [0, 1],
    'c': [7, -2],
    'd': [5, -1],
    'e': [0, 0],
    'f': [-4, 6]
]

def dataList = [
    [1, 2],
    [0, 1],
    [7, -2],
    [5, -1],
    [0, 0],
    [-4, 6]
]


// 1. sort + keySet - сортировка по сумме элементов, использовать dataMap для преобразования

def resultData

// Решение

resultData = dataMap.sort {it.value.sum()}.keySet()

assert resultData as List == ['e', 'b', 'f', 'a', 'd', 'c'] as List

// 2. collect - произведение элементов, использовать dataMap для преобразования

// Решение

resultData  = dataMap.collect { it.value.get(0)*it.value.get(1)}
//resultData = dataMap.collect {it.value.inject(1) {a,b ->a*b}}

assert resultData as List == [2, 0, -14, -5, 0, -24] as List

// 3. findResults - произведение элементов с исключением нулевых, использовать dataList для преобразования

// Решение
resultData = dataList.findResults {it.inject(1) {a, b -> a*b} !=0 ? it.inject(1) {a, b -> a*b}:null}
//resultData = dataList.findResults {!it.contains(0)? it.get(0)*it.get(1):null}

assert resultData as List == [2, -14, -5, -24] as List

// 4. collect + findAll - задание аналогично 3-ему

// Решение
resultData = dataList.collect {it.inject(1) {a, b -> a*b} !=0 ? it.inject(1) {a, b -> a*b}:null}.findAll()

assert resultData as List == [2, -14, -5, -24] as List

// 5. collectEntries + keySet - ключи элементов, произведение которых == 0, должен удвоится, использовать dataMap для преобразования

// Решение

resultData = dataMap.collectEntries {key, value ->value.inject {a,b -> a*b} == 0 ? [key*2, value]:[key, value]}.keySet()

assert resultData as List == ['a', 'bb', 'c', 'd', 'ee', 'f'] as List

// 6. collectMany - в результате должны быть все элементы, использовать dataList для преобразования

// Решение

resultData = dataList.collectMany {it.toList()}

assert resultData as List == [1, 2, 0, 1, 7, -2, 5, -1, 0, 0, -4, 6]

// 7. collectMany + inject - в результате должна быть сумма всех элементов, использовать dataList для преобразования

def sum

// Решение
sum = dataList.collectMany {it.toList()}.inject(0) {a,b -> a + b}

assert sum == 15
